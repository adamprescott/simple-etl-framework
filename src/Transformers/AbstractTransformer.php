<?php

namespace adamprescott\ETL\Transformers;


use adamprescott\ETL\AbstractETL;

abstract class AbstractTransformer implements TransformerInterface
{
    private $ETL;

    public function __construct(AbstractETL $ETL)
    {
        $this->ETL = $ETL;
    }

    protected function setCurrentRecord($record)
    {
        $this->ETL->setCurrentRecord($record);
    }

    protected function appendSkipped($identifier)
    {
        $this->ETL->appendSkipped($identifier);
    }
}