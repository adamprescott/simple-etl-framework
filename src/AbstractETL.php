<?php

namespace adamprescott\ETL;


use adamprescott\ETL\Extractors\ExtractorInterface;
use adamprescott\ETL\Loaders\LoaderInterface;
use adamprescott\ETL\Transformers\TransformerInterface;
use adamprescott\ETL\Validators\ValidatorInterface;

abstract class AbstractETL
{

    /**
     * @var Validators\ValidatorInterface
     */
    protected $validator;

    /**
     * @var ExtractorInterface
     */
    protected $extractor;

    /**
     * @var TransformerInterface
     */
    protected $transformer;

    /**
     * @var LoaderInterface
     */
    protected $loader;

    /**
     * @var array - Each Item contains the file offset
     */
    protected $skipped;

    protected $curRecord;

    /**
     * AbstractETL constructor.
     * @param LoaderInterface $loader : A loader is required
     * @param string|null $extractorDSN : If the extractor is file based, this will be the path to the file, otherwise this will be DSN string most relevant to to data-source
     * @param ExtractorInterface|null $extractor : If left null, this will a resolve a class based on the calling ETL class, e.g. StockCsv resolves to StockCsvExtractor
     * @param TransformerInterface|null $transformer : If left null, this will a resolve a class based on the calling ETL class, e.g. StockCsv resolves to StockCsvTransformer
     * @param ValidatorInterface|null $validator : If left null, this will a resolve a class based on the calling ETL class, e.g. StockCsv resolves to StockCsvValidator
     */
    public function __construct(
        LoaderInterface $loader,
        $extractorDSN = null,
        ExtractorInterface $extractor = null,
        TransformerInterface $transformer = null,
        ValidatorInterface $validator = null
    ) {

        $this->loader = $loader;
        $this->skipped = [];

        if (is_null($extractor)) {
            $this->extractor = $this->resolveExtractor($extractorDSN);
        } else {
            $this->extractor = $extractor;
        }

        if (is_null($transformer)) {
            $transformer = $this->resolveClassPath('Transformer');
            $this->transformer = new $transformer($this);
        } else {
            $this->transformer = $transformer;
        }

        if (is_null($validator)) {
            $validator = $this->resolveClassPath('Validator');
            $this->validator = new $validator;
        } else {
            $this->validator = $validator;
        }

    }

    private function resolveClassPath($type)
    {
        $explodedClass = explode('\\', get_class($this));
        $class = end($explodedClass);
        $resolved = sprintf('adamprescott\\ETL\\%s\\%s', $type.'s', $class.$type);
        if (!class_exists($resolved)) {
            throw new \InvalidArgumentException('Class "'.$resolved.'" does not exist');
        }

        return $resolved;
    }

    /**
     * @param $extractorDSN
     * @return ExtractorInterface
     */
    private function resolveExtractor($extractorDSN)
    {
        $resolved = $this->resolveClassPath('Extractor');
        return new $resolved($extractorDSN);
    }

    /**
     * @return array An arrary of IDs or file-offsets that were skipped and require manual normalisation.
     */
    public function getSkipped()
    {
        return $this->skipped;
    }

    /**
     * @param $record array Used by a Transformer to set the current record being processed.
     * @return $this
     */
    public function setCurrentRecord($record)
    {
        $this->curRecord = $record;
        return $this;
    }

    /**
     * @param $identifier mixed Used by a Transformer to update the IDs or file-offsets there where skipped and need manual normalisation
     * @return $this
     */
    public function appendSkipped($identifier)
    {
        $this->skipped[] = $identifier;
        return $this;
    }

    /**
     * @return \Iterator Returns an Iterable object containing the source records.
     */
    public function extract()
    {
        return $this->extractor->extract();
    }

    /**
     * @param $record
     * @param $identifier
     * @return boolean|array Returns true if a record was successfully transformed, or an array of failures if not.
     */
    public function transform($record, $identifier)
    {
        return $this->transformer->transform($record, $identifier, $this->validator);
    }

    public function load()
    {
        // TODO: Document Function
        return $this->loader->load($this->curRecord);
    }
}