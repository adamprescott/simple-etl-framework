<?php

namespace adamprescott\ETL\Validators;


interface ValidatorInterface
{
    public function validate($record);
}