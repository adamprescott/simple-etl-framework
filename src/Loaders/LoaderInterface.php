<?php

namespace adamprescott\ETL\Loaders;


interface LoaderInterface
{
    public function load($record);
}