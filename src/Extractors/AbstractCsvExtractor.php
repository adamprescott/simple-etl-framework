<?php

namespace adamprescott\ETL\Extractors;


use League\Csv\Reader;

abstract class AbstractCsvExtractor implements ExtractorInterface
{
    /**
     * @var Reader
     */
    protected $reader;

    public function __construct($path)
    {
        $this->setReader(
            Reader::createFromPath($path, 'r')
                ->setHeaderOffset(0)
        );
    }

    public function setReader(Reader $reader)
    {
        $this->reader = $reader;
    }

    public function extract()
    {
        return $this->reader->getRecords();
    }
}