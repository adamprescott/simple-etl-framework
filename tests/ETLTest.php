<?php

class ETLTest extends \PHPUnit\Framework\TestCase
{
    public function createWithEchoer()
    {
        $loader = new \adamprescott\ETL\Loaders\Echoer();
        $this->assertInstanceOf(\adamprescott\ETL\Loaders\Echoer::class, $loader);

        $stockCSV = new \adamprescott\ETL\StockCsv($loader, __DIR__ . '/data/stock-condensed.csv');
        $this->assertInstanceOf(\adamprescott\ETL\StockCsv::class, $stockCSV);

        return $stockCSV;
    }

    public function testBadFile()
    {
        $this->expectException(League\Csv\Exception::class);
        $stockCSV = new \adamprescott\ETL\StockCsv(new \adamprescott\ETL\Loaders\Echoer(), 'BADFilePath.csv');
        $this->assertInstanceOf(\adamprescott\ETL\StockCsv::class, $stockCSV);
    }

    public function testSuccessfulETL()
    {
        $stockCSV = $this->createWithEchoer();
        $stream = $stockCSV->extract();
        $this->assertInstanceOf(Iterator::class, $stream);

        $stream->next();
        $goodRecord = $stream->current();
        $transform = $stockCSV->transform($goodRecord, 1);
        $this->assertTrue($transform);


        $curRecord = $this->getCurRecord($stockCSV);
        $this->assertEquals(399.99, $curRecord['Cost in GBP']);
        $this->assertEquals(false, $curRecord['Discontinued']);
        $this->assertEquals(10, $curRecord['Stock']);

        $this->assertEquals(true, $stockCSV->load());
    }

    public function testBadRecord()
    {
        $stockCSV = $this->createWithEchoer();
        $stream = $stockCSV->extract();
        $stream->next();
        $stream->next();

        $badRecord = $stream->current();
        $transformError = $stockCSV->transform($badRecord,1);
        $this->assertArrayHasKey('Stock', $transformError);
        $this->assertEquals(1, count($stockCSV->getSkipped()));
    }

    public function testETLResolveException()
    {
        $reflection = new ReflectionMethod(\adamprescott\ETL\StockCsv::class, 'resolveClassPath');
        $reflection->setAccessible(true);

        $this->expectException(InvalidArgumentException::class);
        $reflection->invokeArgs($this->createWithEchoer(), ['badType']);
    }

    public function testCodeCoverage()
    {
        $stockCSV = new \adamprescott\ETL\StockCsv(
            new \adamprescott\ETL\Loaders\Echoer(),
            null,
            new \adamprescott\ETL\Extractors\StockCsvExtractor(__DIR__.'/data/stock-condensed.csv'),
            new \adamprescott\ETL\Transformers\StockCsvTransformer($this->createMock(\adamprescott\ETL\StockCsv::class)),
            new \adamprescott\ETL\Validators\StockCsvValidator()
        );

        $this->assertInstanceOf(\adamprescott\ETL\StockCsv::class, $stockCSV);
    }

    public function validatorData()
    {
        $longDescription = '';
        for ($i=0;$i < 261;$i++) {$longDescription .= 'a';}
        return [
            ['Product Code', null, false, ['null must contain only letters (a-z) and digits (0-9)', 'null must have a length between 1 and 10']],
            ['Product Code', '£$%$%^£$%^', false, ['"£$%$%^£$%^" must contain only letters (a-z) and digits (0-9)']],
            ['Product Code', 'abcdef123456', false, ['"abcdef123456" must have a length between 1 and 10']],
            ['Product Code', 'abcdef1234', true, null],

            ['Product Name', [1234], false, ['{ 1234 } must be a string']],
            ['Product Name', ['hello'], false, ['{ "hello" } must be a string']],

            ['Product Description', null, true, null],
            ['Product Description', json_decode('{"object": true}'), false, ['`[object] (stdClass: { "object": true })` must be a string']],
            ['Product Description', $longDescription, false, ['"aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa" must have a length lower than 260']],

            ['Stock', 3.14, false, ['3.14 must be an integer number']],
            ['Stock', 12, true, null],

            ['Cost in GBP', 11.99, true, null],
            ['Cost in GBP', '21.99', true, null],
            ['Cost in GBP', json_decode('{"object": true}'), false, ['`[object] (stdClass: { "object": true })` must be numeric']],

            ['Discontinued', true, true, null],
            ['Discontinued', false, true, null],
            ['Discontinued', 'BAD VALUE', false, ['"BAD VALUE" must be a boolean value']],
        ];
    }

    /**
     * @dataProvider validatorData
     */
    public function testValidator($testField, $testValue, $isValid, $expectedMessages)
    {
        $validator = new \adamprescott\ETL\Validators\StockCsvValidator();

        $validate = $validator->validateField($testField, $testValue);
        $this->assertInstanceOf(\adamprescott\ETL\Validators\Result::class, $validate);
        $this->assertEquals($isValid, $validate->getResult());
        $this->assertEquals($expectedMessages, $validate->getMessages());

    }

    public function testValidatorException()
    {
        $validator = new \adamprescott\ETL\Validators\StockCsvValidator();

        $this->expectException(InvalidArgumentException::class);
        $validator->validateField('badFieldName', null);
    }

    public function testPDOLoader()
    {
        $PDO = new PDO('sqlite::memory:');

        $createTable = <<<SQL
CREATE TABLE IF NOT EXISTS main.products (
  product_code varchar(10) PRIMARY KEY,
  product_name varchar(140),
  description varchar(260) NULL,
  stock integer,
  cost double,
  discontinued integer
);
SQL;
        $this->assertEquals(0, $PDO->exec($createTable));

        $loader = new \adamprescott\ETL\Loaders\StockCsvPDOLoader($PDO);
        $this->assertInstanceOf(\adamprescott\ETL\Loaders\StockCsvPDOLoader::class, $loader);

        $goodRecord = [
            'Product Code' => 'P0001',
            'Product Name' => 'TV',
            'Product Description' => '32” Tv',
            'Stock' => 10,
            'Cost in GBP' => 399.99,
            'Discontinued' => false
        ];

        /** @var \adamprescott\ETL\Validators\Result $result */
        $result = $loader->load($goodRecord);

        $this->assertEquals(true, $result->getResult());
        $this->assertEquals(['00000', null, null], $result->getMessages());

        // Check duplicate
        $duplicateResult = $loader->load($goodRecord);
        $this->assertEquals(false, $duplicateResult->getResult());
        $this->assertEquals(['23000', 19, 'UNIQUE constraint failed: products.product_code'], $duplicateResult->getMessages());

    }

    protected function getCurRecord(\adamprescott\ETL\StockCsv $stockCSV)
    {
        $reflection = new ReflectionClass($stockCSV);
        $curRecordProp = $reflection->getProperty('curRecord');
        $curRecordProp->setAccessible(true);
        return $curRecordProp->getValue($stockCSV);
    }
}
