<?php

namespace adamprescott\ETL\Extractors;


interface ExtractorInterface
{
    public function extract();
}