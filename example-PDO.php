<?php
require_once './vendor/autoload.php';

$sqliteDB = 'example-PDO.sqlite';

if (!file_exists($sqliteDB)) {
    echo 'Creating SQLite file at '.$sqliteDB.PHP_EOL;
    if (!touch($sqliteDB)) {
        exit('Could not create SQLite file at '.$sqliteDB.PHP_EOL);
    }
}

$PDO = new PDO('sqlite:'.$sqliteDB);
$createTable = <<<SQL
CREATE TABLE IF NOT EXISTS main.products (
  product_code varchar(10) PRIMARY KEY,
  product_name varchar(140),
  description varchar(260) NULL,
  stock integer,
  cost double,
  discontinued integer
);
SQL;
$exec = $PDO->exec($createTable);

$loader = new \adamprescott\ETL\Loaders\StockCsvPDOLoader($PDO);
$stockCSV = new adamprescott\ETL\StockCsv($loader, 'tests/data/stock-full.csv');

foreach ($stockCSV->extract() as $offset => $record) {
    $transformed = $stockCSV->transform($record, $offset);
    if ($transformed !== true) {
        var_dump($transformed);
        continue;
    }

    /** @var \adamprescott\ETL\Validators\Result $loaded */
    $loaded = $stockCSV->load();
    if (!$loaded->getResult()) {
        var_dump($loaded->getMessages());
    }
}

echo 'Skipped due to failures: '.count($stockCSV->getSkipped()).PHP_EOL;
echo 'Offsets in need of manual normalisation:'.PHP_EOL.print_r($stockCSV->getSkipped(), true);

