<?php

namespace adamprescott\ETL\Transformers;


use adamprescott\ETL\Validators\ValidatorInterface;

class StockCsvTransformer extends AbstractTransformer
{
    public function transform($record, $offset, $validator = null)
    {
        $record['Discontinued'] = $record['Discontinued'] == 'yes' ? true : false;

        if ($validator instanceof ValidatorInterface) {
            $failures = $validator->validate($record);
            if (count($failures) > 0) {
                $this->appendSkipped($offset);
                return $failures;
            }
        }

        $record['Stock'] = (int) $record['Stock'];
        $record['Cost in GBP'] = (float) $record['Cost in GBP'];

        $this->setCurrentRecord($record);

        return true;
    }
}