<?php

namespace adamprescott\ETL\Validators;


class Result
{
    /**
     * @var boolean
     */
    protected $result;

    /**
     * @var array|null
     */
    protected $messages;

    public function __construct($result, $messages)
    {
        $this->result = $result;
        $this->messages = $messages;
    }

    public function getMessages()
    {
        return $this->messages;
    }

    public function getResult()
    {
        return $this->result;
    }
}