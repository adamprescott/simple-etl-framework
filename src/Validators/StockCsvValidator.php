<?php

namespace adamprescott\ETL\Validators;

use Respect\Validation\Validator as v;

class StockCsvValidator extends AbstractValidator
{

    public function setValidation()
    {
        $validator = [];
        $validator['Product Code'] = v::alnum()->length(1, 10);
        $validator['Product Name'] = v::stringType()->length(1, 140);
        $validator['Product Description'] = v::optional(v::stringType()->length(null, 260));
        $validator['Stock'] = v::intVal();
        $validator['Cost in GBP'] = v::numeric();
        $validator['Discontinued'] = v::boolVal();
        $this->validator = $validator;
        return $this;
    }

}