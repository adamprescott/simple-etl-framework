<?php

namespace adamprescott\ETL\Transformers;


interface TransformerInterface
{
    public function transform($record, $identifier, $validator);
}