# Simple ETL Framework
[![Build Status](https://scrutinizer-ci.com/b/adamprescott/simple-etl-framework/badges/build.png?b=master)](https://scrutinizer-ci.com/b/adamprescott/simple-etl-framework/build-status/master)
[![Scrutinizer Code Quality](https://scrutinizer-ci.com/b/adamprescott/simple-etl-framework/badges/quality-score.png?b=master)](https://scrutinizer-ci.com/b/adamprescott/simple-etl-framework/?branch=master)
[![Code Coverage](https://scrutinizer-ci.com/b/adamprescott/simple-etl-framework/badges/coverage.png?b=master)](https://scrutinizer-ci.com/b/adamprescott/simple-etl-framework/?branch=master)

For sample usage, see both `example.php` and `example-PDO.php` files.

## example-PDO.php
This example creates a sqlite file and will import the data provided in `tests/data/stock-full.csv`

# Usage
This framework can be extended to handle performing ETLs against various data-sets the project layout consists of:

* **Extractors** - This should contain classes for extracting from various data-sources, the current example given in the
    directory is for a CSV data-source though adding ones for PDO, Parquet, etc... should be trivial.
* **Transformers** - This should contain classes for transforming and triggering validation against data provided by an
    extractor.
* **Validators** - This should contain classes for performing validation against the Extracted data, this should be applied
    as part of the transformation process.
* **Loaders** - This should contain classes for loading transformed data to a target datasource.

## AbstractETL.php
This acts as the glue for all of the above classes, provided as an example is the `StockCsv.php` file. The `AbstractETL.php`
class automatically takes care of resolving the various classes mentioned above based on their class name e.g. `StockCsv`
resolves to: 

* StockCsvExtractor
* StockCsvTransformer
* StockCsvValidator

### Todo
* Add support for other datasources (e.g. Parquet, JSON)
* Abstract PDO
* Add buffered loading
* Add concurrent Extractors processing/promises (ReactPHP?)