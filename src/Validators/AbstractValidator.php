<?php

namespace adamprescott\ETL\Validators;

use Respect\Validation\Exceptions\NestedValidationException;
use Respect\Validation\Validator;

abstract class AbstractValidator implements ValidatorInterface
{
    /**
     * @var Validator[]
     */
    protected $validator;

    public function __construct()
    {
        $this->setValidation();
    }

    abstract public function setValidation();

    public function validateField($field, $value)
    {
        if (!key_exists($field, $this->validator)) {
            throw new \InvalidArgumentException('Field "'.$field.'" not found');
        }

        try {
            return new Result(
                $this->validator[$field]->assert($value),
                null
            );
        } catch (NestedValidationException $exception) {
            return new Result(false, $exception->getMessages());
        }
    }

    public function validate($record)
    {
        $failures = [];
        foreach ($record as $key => $value) {
            $isValid = $this->validateField($key, $value);
            if (!$isValid->getResult()) {
                $failures[$key] = $isValid->getMessages();
            }
        }

        return $failures;
    }
}