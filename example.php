<?php
require_once './vendor/autoload.php';

$loader = new \adamprescott\ETL\Loaders\Echoer();
$stockCSV = new adamprescott\ETL\StockCsv($loader, 'test/data/stock-full.csv');

foreach ($stockCSV->extract() as $offset => $record) {
    $transformed = $stockCSV->transform($record, $offset);
    if ($transformed !== true) {
        var_dump($transformed);
        continue;
    }
    $loaded = $stockCSV->load();
}

echo 'Skipped due to failures: '.count($stockCSV->getSkipped()).PHP_EOL;
echo 'Offsets in need of manual normalisation:'.PHP_EOL.print_r($stockCSV->getSkipped(), true);