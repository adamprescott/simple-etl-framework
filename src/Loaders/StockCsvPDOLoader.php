<?php

namespace adamprescott\ETL\Loaders;


use adamprescott\ETL\Validators\Result;

class StockCsvPDOLoader implements LoaderInterface
{
    /**
     * @var \PDO
     */
    protected $PDO;

    public function __construct(\PDO $PDO)
    {
        $this->PDO = $PDO;
    }

    /**
     * @param $record
     * @return Result
     */
    public function load($record)
    {
        // TODO: Add batch size processing rather than inserting each record.
        $prepInsert = $this->PDO->prepare('INSERT INTO products 
          (product_code, product_name, description, stock, cost, discontinued) VALUES 
          (:product_code, :product_name, :description, :stock, :cost, :discontinued)');

        $status = $prepInsert->execute([
            ':product_code' => $record['Product Code'],
            ':product_name' => $record['Product Name'],
            ':description' => $record['Product Description'],
            ':stock' => $record['Stock'],
            ':cost' => $record['Cost in GBP'],
            ':discontinued' => $record['Discontinued']
        ]);

        return new Result($status, $prepInsert->errorInfo());
    }
}